import * as React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import VisuallyHidden from '@reach/visually-hidden';

import { themeProps, Box, UnstyledButton, Text } from 'components/design-system';
import { logEventClick } from 'utils/analytics';
import useDarkMode from 'utils/useDarkMode';

import Logo from './Logo';
import {
  NavGrid,
  MainNavInner,
  MainNavCenter,
  SecondaryNavWrapper,
  SecondaryNavLink,
  MainNavLink,
  MobileNav,
  MobileNavLink,
  MainNavCenterLinks,
  MainNavRight,
} from './components';
import OptionModal from './OptionModal';
import {
  HomeIcon,
  InformationIcon,
  // CaseIcon,
  // GuideIcon,
  OptionIcon,
  ChevronIcon,
  SearchIcon,
} from '../../icons';

import SearchModal from '../../../modules/core/SearchModal';

interface NavigationProps {
  pageTitle?: string;
}

const Root = Box.withComponent('header');

const LogoLinkRoot = Box.withComponent('a');

const LogoLink = styled(LogoLinkRoot)`
  display: block;
  width: 56px;
  height: 48px;
  overflow: hidden;

  ${themeProps.mediaQueries.md} {
    width: 80px;
    height: 64px;
  }

  &:hover,
  &:focus,
  &:active,
  &:visited {
    text-decoration: none;
  }
`;

const LogoWrapper = styled(Box)`
  > svg {
    ${themeProps.mediaQueries.md} {
      width: 80px !important;
      height: 64px !important;
    }
  }
`;

const ColorToggleWrapper = styled(Box)`
  display: none;
  justify-content: space-between;
  align-items: center;

  ${themeProps.mediaQueries.sm} {
    display: flex;
  }
`;

const ToggleButtonText = styled(Text)`
  display: none;

  ${themeProps.mediaQueries.lg} {
    display: block;
  }
`;

const ToggleButtonInnerWrapper = styled.div`
  display: flex;
  width: 134px;
  height: 32px;
  font-weight: bold;
  line-height: 1;
`;

const ToggleButtonLight = styled(Box)`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  border-radius: 16px 0 0 16px;
`;

const ToggleButtonDark = styled(Box)`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  border-radius: 0 16px 16px 0;
`;

const SearchButton = styled(UnstyledButton)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  outline: none;
`;

const OptionButton = styled(UnstyledButton)`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
  font-size: 10px;
  margin-bottom: 4px;
`;

const OptionButtonIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  margin-bottom: 4px;
`;

const PageTitle = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${themeProps.mediaQueries.sm} {
    display: none;
  }
`;

const Navigation: React.FC<NavigationProps> = ({ pageTitle }) => {
  const [isDarkMode, toggleDarkMode] = useDarkMode();
  const [isSearchModalOpen, setIsSearchModalOpen] = React.useState(false);
  const [isOptionModalOpen, setIsOptionModalOpen] = React.useState(false);
  const router = useRouter();

  const toggleSearchModal = () => {
    setIsSearchModalOpen(!isSearchModalOpen);
  };

  const toggleOptionModal = () => {
    setIsOptionModalOpen(!isOptionModalOpen);
  };

  return (
    <Root>
      <NavGrid>
        <MainNavInner>
          <Link href="/" passHref>
            <LogoLink onClick={() => logEventClick('Beranda')}>
              <VisuallyHidden>Kawal COVID-19</VisuallyHidden>
              <LogoWrapper display="flex" alignItems="flex-end">
                <Logo aria-hidden />
              </LogoWrapper>
            </LogoLink>
          </Link>
          <MainNavCenter flex="1 1 auto">
            {pageTitle && (
              <PageTitle>
                <ChevronIcon fill={themeProps.colors.accents07} />
                <Text ml="sm">{pageTitle}</Text>
              </PageTitle>
            )}
            <MainNavCenterLinks>
              <Link href="/" passHref>
                <MainNavLink isActive={router.pathname === '/'}>Beranda</MainNavLink>
              </Link>
              <Link href="/category/[slug]" as="/category/informasi" passHref>
                <MainNavLink
                  isActive={router.pathname === '/category/[slug]' || router.pathname === '/faq'}
                >
                  Informasi
                </MainNavLink>
              </Link>
              {/* <Link href="/" passHref>
                <MainNavLink>Kasus</MainNavLink>
              </Link> */}
              {/* <Link href="/category/[slug]" as="/category/panduan" passHref>
                  <MainNavLink
                    isActive={
                      router.pathname === '/category/[slug]' && router.asPath === '/category/panduan'
                    }
                  >
                    Panduan
                  </MainNavLink>
                </Link> */}
              {/* <Link href="/" passHref>
                <MainNavLink>Lainnya</MainNavLink>
              </Link> */}
              <Link href="/unduh-aplikasi" passHref>
                <MainNavLink isActive={router.pathname === '/unduh-aplikasi'}>
                  Unduh Aplikasi
                </MainNavLink>
              </Link>
              <Link href="/tentang-kami" passHref>
                <MainNavLink isActive={router.pathname === '/tentang-kami'}>
                  Tentang Kami
                </MainNavLink>
              </Link>
            </MainNavCenterLinks>
          </MainNavCenter>
          <MainNavRight display="flex" alignItems="center">
            <ColorToggleWrapper>
              <ToggleButtonText variant={200} mr="sm">
                Mode warna
              </ToggleButtonText>
              <UnstyledButton style={{ outline: 'none' }} onClick={toggleDarkMode}>
                <ToggleButtonInnerWrapper>
                  <ToggleButtonLight
                    backgroundColor={isDarkMode ? 'accents03' : 'brandred'}
                    color={isDarkMode ? 'foreground' : 'white'}
                  >
                    <Text variant={200}>Terang</Text>
                  </ToggleButtonLight>
                  <ToggleButtonDark
                    backgroundColor={isDarkMode ? 'brandred' : 'accents01'}
                    color="foreground"
                  >
                    <Text variant={200}>Gelap</Text>
                  </ToggleButtonDark>
                </ToggleButtonInnerWrapper>
              </UnstyledButton>
            </ColorToggleWrapper>
            <SearchButton
              type="button"
              backgroundColor="accents01"
              onClick={toggleSearchModal}
              display={['none', null, 'flex', null, null]}
              ml="md"
            >
              <VisuallyHidden>Pencarian</VisuallyHidden>
              <SearchIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} aria-hidden />
            </SearchButton>
          </MainNavRight>
        </MainNavInner>
      </NavGrid>
      <SecondaryNavWrapper
        backgroundColor="accents02"
        color="foreground"
        display={
          router.pathname === '/category/[slug]' || router.pathname === '/faq' ? 'flex' : 'none'
        }
      >
        <NavGrid flex="1 1 auto" backgroundColor="accents02" color="foreground">
          <Box
            as="nav"
            display="flex"
            flexDirection="row"
            gridColumn="3/4"
            overflowX="auto"
            overflowY="hidden"
          >
            <SecondaryNavLink
              href="/category/[slug]"
              as="/category/informasi"
              isActive={router.query.slug === 'informasi'}
              title="Informasi Terkini"
            />
            <SecondaryNavLink
              href="/category/[slug]"
              as="/category/panduan"
              isActive={router.query.slug === 'panduan'}
              title="Panduan"
            />
            <SecondaryNavLink
              href="/category/[slug]"
              as="/category/bacaan"
              isActive={router.query.slug === 'bacaan'}
              title="Bacaan Pilihan"
            />
            <SecondaryNavLink
              href="/category/[slug]"
              as="/category/verifikasi"
              isActive={router.query.slug === 'verifikasi'}
              title="Periksa Fakta"
            />
            <SecondaryNavLink
              href="/category/[slug]"
              as="/category/checklist"
              isActive={router.query.slug === 'checklist'}
              title="Check-list"
            />
            <SecondaryNavLink
              href="/category/[slug]"
              as="/category/infografik"
              isActive={router.query.slug === 'infografik'}
              title="Infografik"
            />
            <SecondaryNavLink href="/faq" isActive={router.pathname === '/faq'} title="FAQ" />
          </Box>
        </NavGrid>
      </SecondaryNavWrapper>
      <NavGrid backgroundColor="accents02" color="foreground">
        <MobileNav backgroundColor={isDarkMode ? 'accents03' : '#fff'}>
          <MobileNavLink
            href="/"
            isActive={router.pathname === '/'}
            title="Beranda"
            icon={<HomeIcon fill={isDarkMode || router.pathname === '/' ? '#f1f2f3' : '#22272c'} />}
          />
          <MobileNavLink
            href="/category/[slug]"
            as="/category/informasi"
            isActive={router.pathname === '/category/[slug]' || router.pathname === '/faq'}
            title="Informasi"
            icon={
              // eslint-disable-next-line react/jsx-wrap-multilines
              <InformationIcon
                fill={
                  isDarkMode || router.pathname === '/category/[slug]' || router.pathname === '/faq'
                    ? '#f1f2f3'
                    : '#22272c'
                }
              />
            }
          />
          {/* <NavLinkMobile
            href="/ongoing"
            isActive={router.pathname === '/kasus'}
            title="Kasus"
            icon={<CaseIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} />}
          /> */}
          {/* <NavLinkMobile
            href="/ongoing"
            isActive={router.pathname === '/category/[slug]'}
            title="Panduan"
            icon={<GuideIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} />}
          /> */}
          <OptionButton type="button" style={{ outline: 'none' }} onClick={toggleOptionModal}>
            <OptionButtonIcon>
              <OptionIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} />
            </OptionButtonIcon>
            Lainnya
          </OptionButton>
        </MobileNav>
      </NavGrid>
      <SearchModal isOpen={isSearchModalOpen} onClose={toggleSearchModal} />
      <OptionModal isOpen={isOptionModalOpen} onClose={toggleOptionModal} />
    </Root>
  );
};

export default Navigation;
