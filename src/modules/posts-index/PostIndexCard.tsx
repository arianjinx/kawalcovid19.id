import * as React from 'react';
import Link from 'next/link';
import convert from 'htmr';
import styled from '@emotion/styled';

import { Box, Stack, Heading, Text, UnstyledAnchor } from 'components/design-system';
import { WordPressPostIndex, WordPressUser } from 'types/wp';
import formatTime from 'utils/formatTime';
import { logEventClick } from 'utils/analytics';
import htmrTransform from './utils/htmrTransform';

import { PostIndexAuthor } from './components';

interface PostIndexCardProps {
  post: WordPressPostIndex;
  author?: WordPressUser;
  hasExcerpt?: boolean;
}

const PostLink = styled(UnstyledAnchor)`
  box-shadow: unset;

  &:hover,
  &:focus,
  &:active,
  &:visited {
    box-shadow: unset;
  }

  &:hover,
  &:focus {
    h3 {
      text-decoration: underline;
    }
  }
`;

const PostIndexCard: React.FC<PostIndexCardProps> = ({ post, author, hasExcerpt }) => {
  const { id, title, excerpt, date_gmt, slug } = post;

  return (
    <Box as="article" display="flex" flexDirection="column" bg="accents01" borderRadius={8}>
      <Link href="/content/[...paths]" as={`/content/${id}/${slug}`} passHref>
        <PostLink
          flex="1 1 auto"
          onClick={() => logEventClick(title.rendered)}
          aria-label={title.rendered}
        >
          <Stack spacing="md" p="md">
            <Box as="header">
              <Heading variant={600} as="h3">
                {convert(title.rendered)}
              </Heading>
            </Box>
            {hasExcerpt ? (
              <Box as="section">
                {convert(excerpt.rendered, {
                  transform: htmrTransform,
                })}
              </Box>
            ) : null}
          </Stack>
        </PostLink>
      </Link>
      <Box as="footer" display="flex" flexDirection="row" alignItems="center" p="md" pt={0}>
        {author && <PostIndexAuthor author={author} />}
        <Box
          display="flex"
          flexDirection="row"
          alignItems="center"
          justifyContent="flex-end"
          flex="1 1 auto"
          height={24}
        >
          <Text variant={300} fontFamily="monospace">
            <time dateTime={date_gmt}>{formatTime(new Date(date_gmt))}</time>
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

PostIndexCard.defaultProps = {
  hasExcerpt: false,
};

export default PostIndexCard;
