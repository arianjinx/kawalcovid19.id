import * as React from 'react';
import App from 'next/app';
import Head from 'next/head';
import Router from 'next/router';
import { CacheProvider } from '@emotion/core';
import { cache } from 'emotion';
import NProgress from 'nprogress';
import * as Sentry from '@sentry/browser';

import { ThemeWrapper } from 'components/layout';
import { logException } from 'utils/analytics';

import 'typeface-ibm-plex-sans';
import 'typeface-ibm-plex-mono';
import 'modern-normalize';
import '@wordpress/block-library/build-style/style.css';

const progress = NProgress.configure({ showSpinner: false });

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  whitelistUrls: [/kawalcovid19\.id/i],
});

export default class MyApp extends App {
  public componentDidMount() {
    // NProgress
    Router.events.on('routeChangeStart', () => progress.start());
    Router.events.on('routeChangeComplete', () => progress.done());
    Router.events.on('routeChangeError', () => progress.done());
  }

  public componentWillUnmount() {
    // NProgress
    Router.events.off('routeChangeStart', () => progress.start());
    Router.events.off('routeChangeComplete', () => progress.done());
    Router.events.off('routeChangeError', () => progress.done());
  }

  public componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    logException(error.message, true);

    Sentry.withScope(scope => {
      scope.setExtras(errorInfo);
      const eventId = Sentry.captureException(error);
      this.setState({ eventId });
    });

    super.componentDidCatch(error, errorInfo);
  }

  public render() {
    const { Component, pageProps } = this.props;

    return (
      <CacheProvider value={cache}>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        </Head>
        <ThemeWrapper>
          <Component {...pageProps} />
        </ThemeWrapper>
      </CacheProvider>
    );
  }
}
