import * as React from 'react';
import { NextPage } from 'next';

import { wp } from 'utils/api';
import { WordPressPostIndex, WordPressUser } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { Heading, Stack, Text, Box } from 'components/design-system';
import { PostIndexCard } from 'modules/posts-index';
import {
  Hero,
  ArticlesListGrid,
  ImportantLinksSection,
  SocialMediaSection,
  CasesSection,
} from 'modules/home';
import getAuthorsDetail from 'utils/wp/getAuthorsDetail';
import styled from '@emotion/styled';
import Link from 'next/link';
import { logEventClick } from 'utils/analytics';

interface IndexPageProps {
  information?: WordPressPostIndex[];
  infographic?: WordPressPostIndex[];
  verification?: WordPressPostIndex[];
  authors?: Record<number, WordPressUser>;
  errors?: string;
}

const Section = Content.withComponent('section');

const TextLink = Text.withComponent('a');

const ReadmoreLink = styled(TextLink)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

export interface CategorySectionProps {
  posts?: WordPressPostIndex[];
  authors?: Record<number, WordPressUser>;
  slug: string;
  title: string;
  learnMore: string;
  hasExcerpt?: boolean;
}

const CategorySection: React.FC<CategorySectionProps> = ({
  posts,
  authors,
  slug,
  title,
  learnMore,
  hasExcerpt,
}) =>
  posts?.length ? (
    <Stack spacing="xl" mb="xxl">
      <Stack spacing="xl" mb="lg">
        <Heading variant={800} as="h2">
          {title}
        </Heading>
        <ArticlesListGrid>
          {posts.map(post => (
            <PostIndexCard
              key={post.slug}
              post={post}
              author={authors?.[post.author]}
              hasExcerpt={hasExcerpt}
            />
          ))}
        </ArticlesListGrid>
      </Stack>
      <Box>
        <Link href="/category/[slug]" as={`/category/${slug}`} passHref>
          <ReadmoreLink variant={500} color="primary02" onClick={() => logEventClick(learnMore)}>
            {learnMore} &rarr;
          </ReadmoreLink>
        </Link>
      </Box>
    </Stack>
  ) : null;

const IndexPage: NextPage<IndexPageProps> = ({
  information,
  infographic,
  verification,
  authors,
}) => (
  <PageWrapper pageTitle="Beranda">
    <Hero />
    <Section>
      <Column>
        <Stack spacing="xxl">
          <CasesSection />
          <CategorySection
            posts={information}
            authors={authors}
            slug="informasi"
            title="Informasi Terkini"
            learnMore="Lihat Semua Berita"
            hasExcerpt
          />
          <CategorySection
            posts={infographic}
            authors={authors}
            slug="infografik"
            title="Infografik Terbaru"
            learnMore="Lihat Semua Infografik"
          />
          <CategorySection
            posts={verification}
            authors={authors}
            slug="verifikasi"
            title="Periksa Fakta"
            learnMore="Lihat Semua Fakta"
          />
          <ImportantLinksSection />
          <SocialMediaSection />
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
);

export async function getStaticProps() {
  try {
    const [informationCategory, infographicCategory, verificationCategory] = await Promise.all([
      wp('wp/v2/categories', {
        slug: 'informasi',
        _fields: 'id,count,description,name,slug',
      }),
      wp('wp/v2/categories', {
        slug: 'infografik',
        _fields: 'id,count,description,name,slug',
      }),
      wp('wp/v2/categories', {
        slug: 'verifikasi',
        _fields: 'id,count,description,name,slug',
      }),
    ]);

    if (informationCategory && infographicCategory && verificationCategory) {
      let information: WordPressPostIndex[] | undefined;
      let infographic: WordPressPostIndex[] | undefined;
      let verification: WordPressPostIndex[] | undefined;
      let authors: Record<string, WordPressUser> | undefined;

      const [
        unfilteredInformationPosts,
        unfilteredInfographicPosts,
        unfilteredVerificationPosts,
      ] = await Promise.all([
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: informationCategory[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: infographicCategory[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: verificationCategory[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
      ]);

      if (Array.isArray(unfilteredInformationPosts)) {
        const posts = unfilteredInformationPosts.filter(post => post.type === 'post');
        information = posts.slice(0, 4);
      }

      if (Array.isArray(unfilteredInfographicPosts)) {
        const posts = unfilteredInfographicPosts.filter(post => post.type === 'post');
        infographic = posts.slice(0, 4);
      }

      if (Array.isArray(unfilteredVerificationPosts)) {
        const posts = unfilteredVerificationPosts.filter(post => post.type === 'post');
        verification = posts.slice(0, 2);
      }

      if (information?.length || infographic?.length || verification?.length) {
        const authorsIdList = [
          ...(information || []),
          ...(infographic || []),
          ...(verification || []),
        ].map(post => post.author);

        const uniqueAuthors = [...new Set(authorsIdList)];
        const authorsDetail = await getAuthorsDetail(uniqueAuthors);
        const map = authorsDetail.reduce<Record<string, WordPressUser>>((obj, item) => {
          // eslint-disable-next-line no-param-reassign
          obj[item.id] = item;
          return obj;
        }, {});

        authors = map;
      }

      return { props: { information, infographic, verification, authors } };
    }

    throw new Error('Failed to fetch posts');
  } catch (err) {
    return { props: { errors: err.message } };
  }
}

export default IndexPage;
